import React from "react";
import { StyleSheet, Text, View, AppState } from "react-native";
import { Updates } from "expo";

export default class App extends React.Component {
  state = {
    loading: true,
    closedAppTime: null
  };

  asyncSetTimeout = () => {
    return new Promise((res, rej) => {
      setTimeout(() => {
        res();
      }, 2000);
    });
  };

  init = async () => {
    this.setState({ loading: true });
    const updatesAvailable = await Updates.checkForUpdateAsync();
    if (updatesAvailable.isAvailable === true) {
      Updates.reload();
    }
    await this.asyncSetTimeout();
    this.setState({ loading: false });
  };

  componentDidMount = () => {
    AppState.addEventListener("change", this.handleAppStateChange);
    this.init();
  };

  componentWillUnmount = async () => {
    AppState.removeEventListener("change", this.handleAppStateChange);
  };

  handleAppStateChange = async nextAppState => {
    console.log("nextAppState", nextAppState);
    if (nextAppState === "active") {
      if (this.state.closedAppTime !== null) {
        const appClosedFor = (Date.now() - this.state.closedAppTime) / 1000;
        console.log("=============appClosedFor=============", appClosedFor);
        if (appClosedFor > 3) {
          await AppState.removeEventListener(
            "change",
            this.handleAppStateChange
          );
          this.setState({ closedAppTime: null });
          this.init();
        }
        this.setState({ closedAppTime: null });
      }
    }
    if (nextAppState.match(/inactive|background/)) {
      if (this.state.closedAppTime === null) {
        this.setState({ closedAppTime: Date.now() });
      }
    }
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <Text>Loading...</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Text>App has been loaded</Text>
        <Text>Version 5</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
